import pytest
from pytest import ExitCode
import os
import telebot
from helpers.auth_helper import AuthHelper
from decouple import config


@pytest.fixture(autouse=True, scope="session")
def get_auth_token():
    response = AuthHelper.login(config("LOGIN"), config("PASSWORD"))
    os.environ["TOKEN"] = response.json()["token"]

def pytest_sessionfinish(session, exitstatus):
    if exitstatus is ExitCode.TESTS_FAILED and config('TELEGRAM_REPORTING') == 'true':
        failed_tests = []
        for item in session.items:
            if item.rep_call.failed:
                failed_tests.append(item.name)
        failed_tests_join = "\n".join(
            map(
                lambda single_failed_test: F"> {single_failed_test}",
                failed_tests
            )
        )
        message = F"""tests failed/collected: {session.testsfailed}/{session.testscollected}
        tests failed:
        {failed_tests_join}
        """
        bot = telebot.TeleBot(config("TELEGRAM_BOT_ID"))
        bot.send_message(chat_id=config("TELEGRAM_CHAT_ID"), text=message)

@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
    return rep
