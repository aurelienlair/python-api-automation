# API browser test automation

## Description

This project is an experimental learning of the API automation testing in [Python](https://en.wikipedia.org/wiki/Python_(programming_language)). It tests the authentication on a [dummy website](https://localcoding.us):
<img src="docs/images/dummy_website.png" alt="login" width="400"/>

It uses [Pytest](https://docs.pytest.org/en/7.0.x/) as testing framework.

The outcome of the test suite can be visualized thanks to [Allure](http://allure.qatools.ru/) test automation report.

It also allows to send a mini report of the tests failed on an ad-hoc configurable [Telegram](https://en.wikipedia.org/wiki/Telegram_(software)) chat.

<img src="docs/images/telegram_report.png" alt="login" width="400"/>

Testing can be done either locally or throughout the ad-hoc [Gitlab](https://gitlab.com/aurelienlair/python-api-automation/-/pipelines) [CD/CI](https://en.wikipedia.org/wiki/CI/CD) pipeline.


## Getting Started

The following instructions will allow you to set the project up from scratch.

## System requirements

- [PyCharm Community Edition](https://www.jetbrains.com/pycharm/) as IDE (the latest version)
- [PyEnv](https://github.com/pyenv/pyenv) as Python Version Management (we used 3.10.0 Python version)
- [Allure](https://docs.qameta.io/allure/)

## Allure report

Before generating the report please ensure that while invoking pytest with PyCharm its report
directory is correctly configured:
<img src="docs/images/pytest_allure.png" alt="login" width="400"/>

Then from the terminal run the below command once the test suite has run to generate the reports

```bash
allure generate allure-results -o allure-report
```

Then open it

```bash
allure open
```

<img src="docs/images/allure_report.png" alt="login" width="600"/>

If needed you can use this command to clean the reports

```bash
allure generate --clean
```

### Report in pipeline

You can access the reports (with the history) generated in pipeline with Gitlab pages by opening [this](https://aurelienlair.gitlab.io/python-api-automation/) url.

## Git commit message convention

This projects follows the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

```shell
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Example:

```shell
docs: add description of Python run command
```

| Type | Description |
|------| ----------- |
| `style` | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) |
| `build` | Changes to the build process |
| `chore` | Changes to the build process or auxiliary tools and libraries such as documentation generation |
| `docs` | Documentation updates |
| `feat` | New features |
| `fix`  | Bug fixes |
| `refactor` | Code refactoring |
| `test` | Adding missing tests |
| `perf` | A code change that improves performance |

## Useful links

- [Python W3C documentation](https://www.w3schools.com/python)
- [Code With Me](https://www.jetbrains.com/code-with-me/) is really nice to work in [pair programming](https://en.wikipedia.org/wiki/Pair_programming)
