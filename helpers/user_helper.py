from helpers.api_helper import request


class UserHelper:
    @staticmethod
    def create():
        return request(method="POST", route="/users", authorization=True)

    @staticmethod
    def delete(id: str):
        json_body = {
            "id": id,
        }
        return request(method="DELETE", route="/users", authorization=True, json=json_body)
