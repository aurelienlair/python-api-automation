from helpers.api_helper import request


class AuthHelper:
    @staticmethod
    def login(login: str, password: str):
        json_body = {
            "login": login,
            "password": password
        }
        return request(method="POST", route="/auth", json=json_body)
