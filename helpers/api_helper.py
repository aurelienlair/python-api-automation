import requests
from decouple import config

BASE_URL = config("BASE_URL")


def request(method, route, **kwargs):
    url = BASE_URL + route
    # If authorization key is present it means we need authentication
    if "authorization" in kwargs:
        #TODO clarify why is this for
        if kwargs["authorization"]:
            token = config("TOKEN")
            # If there are some headers we just append the token header in addition to it
            if "headers" in kwargs:
                kwargs["headers"] = kwargs["headers"] | {"Authorization": F"Bearer {token}"}
            # Otherwise we add the token header as unique header
            else:
                kwargs["headers"] = {"Authorization": F"Bearer {token}"}
        # TODO clarify why is this for
        kwargs.pop("authorization")

    return requests.request(method=method, url=url, **kwargs)
