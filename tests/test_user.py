from helpers.user_helper import UserHelper
import allure


@allure.feature("User creation")
class TestCreateUserSuccessfully:
    def setup_class(self):
        self.response = UserHelper.create()
        self.response_body = self.response.json()

    @allure.title("User's creation response code is 200")
    def test_http_status_code_is_200(self):
        assert self.response.status_code == 200

    @allure.title("User's creation response contains id and amount")
    def test_response_body_contains_id_and_amount(self):
        assert "id" in self.response_body
        assert "amount" in self.response_body

    def teardown_class(self):
        self.response = UserHelper.delete(self.response_body.get('id'))
        self.response_body = self.response.json()
        # remove me
        assert self.response.status_code == 200
