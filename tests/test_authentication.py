from helpers.auth_helper import AuthHelper
from decouple import config
import allure


@allure.feature("Authentication")
class TestAuthenticationWithValidCredentials:
    def setup_class(self):
        self.response = AuthHelper.login(config("LOGIN"), config("PASSWORD"))
        self.response_body = self.response.json()

    @allure.title("Authentication response code is 200")
    def test_http_status_code_is_200(self):
        assert self.response.status_code == 200

    @allure.title("Authentication response contains a token")
    def test_token_is_in_the_response_body(self):
        assert "token" in self.response_body
